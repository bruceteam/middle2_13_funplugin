// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Commandlets/Commandlet.h"
#include "PrefixCommandlet.generated.h"


UCLASS()
class LODANDRENAMEPLUGIN_API UPrefixCommandlet : public UCommandlet
{
	GENERATED_BODY()
	
	//start function
	virtual int32 Main(const FString& Params) override;

	//find assets
	void ProcessAssets(TArray<FString> RootDirectories);

	//Seems for ctrl Z
	void SaveAsset(UObject* AssetInstance);

	FString MakePrefix(FString ClassName);
};
