// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Commandlets/Commandlet.h"
#include "LODGeneratorCommandlet.generated.h"


UCLASS()
class LODANDRENAMEPLUGIN_API ULODGeneratorCommandlet : public UCommandlet
{
	GENERATED_BODY()
	
	//start function
	virtual int32 Main(const FString& Params) override;

	//find assets
	void ProcessAssets(TArray<FString> RootDirectories);

	//Logic
	void ModifyLod(UObject* AssetInstance);

	//Seems for ctrl Z
	void SaveAsset(UObject* AssetInstance);
};
