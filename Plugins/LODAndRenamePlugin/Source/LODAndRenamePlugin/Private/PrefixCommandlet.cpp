// Fill out your copyright notice in the Description page of Project Settings.


#include "PrefixCommandlet.h"
#include "Engine/StaticMesh.h"
#include "Modules/ModuleManager.h"
#include "EditorStaticMeshLibrary.h"
#include "AssetRegistryModule.h"
#include "Kismet/KismetSystemLibrary.h"
#include "AssetToolsModule.h"


int32 UPrefixCommandlet::Main(const FString& Params)
{
	TArray<FString> Tokens;
	TArray<FString> Switches;

	ParseCommandLine(*Params, Tokens, Switches);
	//My Engine folder
	// "D:\UE_4.27\Engine\Binaries\Win64\UE4Editor.exe"
	// 
	// my project path 
	// "D:\MiddleVS\Middle2_13\Middle2_13.uproject"
	// 
	// my command
	//-run=ModifyAssets -GenerateLOD /Content/Samples/Meshes/

	//whole string
	//   "D:\UE_4.27\Engine\Binaries\Win64\UE4Editor.exe" "D:\MiddleVS\Middle2_13\Middle2_13.uproject" -run=Prefix -GeneratePrefix /Game/Samples/Materials/    

	if (Switches.Contains(TEXT("GeneratePrefix")))
	{
		if (Tokens.Num() > 0)
		{
			ProcessAssets(Tokens);
		}
	}

	return 0;
}

void UPrefixCommandlet::ProcessAssets(TArray<FString> RootDirectories)
{
	//dinamic adding module
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>//
		(AssetRegistryConstants::ModuleName);

	//find all assets
	AssetRegistryModule.Get().SearchAllAssets(true);
	TArray<FString> ClassNames;

	ClassNames.Add(TEXT("StaticMesh"));
	ClassNames.Add(TEXT("SoundCue"));
	ClassNames.Add(TEXT("SoundBase"));
	ClassNames.Add(TEXT("Texture"));
	ClassNames.Add(TEXT("Material"));
	ClassNames.Add(TEXT("MaterialInstance"));

	for (FString ClassName : ClassNames)
	{
		//FString ClassName = TEXT("MaterialInstance");
		TArray<FAssetData> AssetList;
		AssetRegistryModule.Get().GetAssetsByClass(*ClassName, AssetList, true);

		for (FAssetData AssetData : AssetList)
		{
			for (FString RootDirectory : RootDirectories)
			{
				if (AssetData.ObjectPath.ToString().StartsWith(RootDirectory, ESearchCase::IgnoreCase))
				{
					UObject* AssetInstance = AssetData.GetAsset();
					FString  AssetName = UKismetSystemLibrary::GetObjectName(AssetInstance);
					UE_LOG(LogTemp, Warning, TEXT("AssetName = %s"), *AssetName);
					AssetName = MakePrefix(ClassName) + AssetName;
					AssetInstance->Rename(*AssetName);
					AssetInstance->MarkPackageDirty();
					UE_LOG(LogTemp, Warning, TEXT("AssetName = %s"), *AssetName);

					SaveAsset(AssetInstance);
					//break;
				}
			}
		}
	}
}


void UPrefixCommandlet::SaveAsset(UObject* AssetInstance)
{
	if (!AssetInstance) return;

	if (UPackage* Package = AssetInstance->GetPackage())
	{
		if (Package->IsDirty())
		{
			FString PackageName = FPackageName::LongPackageNameToFilename(Package->GetPathName(),//
				FPackageName::GetAssetPackageExtension());

			UE_LOG(LogClass, Log, TEXT("Saving Asset to: %s"), *PackageName);

			if (Package->SavePackage(Package, AssetInstance, RF_Standalone, *PackageName, GLog))
			{
				UE_LOG(LogClass, Log, TEXT("Done"));
			}
			else
			{
				UE_LOG(LogClass, Log, TEXT("Can't Save Asset-something wrong!"));
			}
		}
	}

}

FString UPrefixCommandlet::MakePrefix(FString ClassName)
{
	if (ClassName == TEXT("StaticMesh"))
	{
		return "SM_";
	}
	else if (ClassName == TEXT("SoundCue"))
	{
		return "SCue_";		
	}
	else if (ClassName == TEXT("SoundBase"))
	{
		return "S_";		
	}
	else if (ClassName == TEXT("Texture"))
	{
		return "T_";		
	}
	else if (ClassName == TEXT("Material"))
	{
		return "M_";		
	}
	else if (ClassName == TEXT("MaterialInstance"))
	{
		return "MI_";		
	}
	else
	{
		return "";
	}
}

