// Fill out your copyright notice in the Description page of Project Settings.


#include "LODGeneratorCommandlet.h"
#include "Engine/StaticMesh.h"
#include "Modules/ModuleManager.h"
#include "EditorStaticMeshLibrary.h"
#include "AssetRegistryModule.h"

int32 ULODGeneratorCommandlet::Main(const FString& Params)
{
	TArray<FString> Tokens;
	TArray<FString> Switches;

	ParseCommandLine(*Params, Tokens, Switches);
	//My Engine folder
	// "D:\UE_4.27\Engine\Binaries\Win64\UE4Editor.exe"
	// 
	// my project path 
	// "D:\MiddleVS\Middle2_13\Middle2_13.uproject"
	// 
	// my command
	//-run=ModifyAssets -GenerateLOD /Content/Samples/Meshes/

	//whole string
	//"D:\UE_4.27\Engine\Binaries\Win64\UE4Editor.exe" "D:\MiddleVS\Middle2_13\Middle2_13.uproject" -run=LODGenerator -GenerateLOD /Game/Samples/Meshes/

	if (Switches.Contains(TEXT("GenerateLOD")))
	{
		if (Tokens.Num() > 0)
		{
			ProcessAssets(Tokens);
		}
	}

	return 0;
}

void ULODGeneratorCommandlet::ProcessAssets(TArray<FString> RootDirectories)
{
	//dinamic adding module
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(AssetRegistryConstants::ModuleName);

	//find all assets
	AssetRegistryModule.Get().SearchAllAssets(true);

	FString ClassName = TEXT("StaticMesh");
	TArray<FAssetData> AssetList;
	AssetRegistryModule.Get().GetAssetsByClass(*ClassName, AssetList, true);

	for (FAssetData AssetData : AssetList)
	{
		for (FString RootDirectory : RootDirectories)
		{
			if (AssetData.ObjectPath.ToString().StartsWith(RootDirectory, ESearchCase::IgnoreCase))
			{
				UObject* AssetInstance = AssetData.GetAsset();

				ModifyLod(AssetInstance);

				SaveAsset(AssetInstance);

				break;
			}
		}
	}
}

void ULODGeneratorCommandlet::ModifyLod(UObject* AssetInstance)
{
	if (UStaticMesh* Mesh = Cast <UStaticMesh>(AssetInstance))
	{
		TArray<FEditorScriptingMeshReductionSettings> ReductionSettings;

		FEditorScriptingMeshReductionSettings Settings;

		//LOD 0
		Settings.PercentTriangles = 1;
		Settings.ScreenSize = 0.9;
		ReductionSettings.Add(Settings);

		//LOD 1
		Settings.PercentTriangles = 0.5;
		Settings.ScreenSize = 0.5;
		ReductionSettings.Add(Settings);

		//LOD 2
		Settings.PercentTriangles = 0.1;
		Settings.ScreenSize = 0.3;
		ReductionSettings.Add(Settings);

		FEditorScriptingMeshReductionOptions Options;
		Options.ReductionSettings = ReductionSettings;

		UEditorStaticMeshLibrary::SetLods(Mesh, Options);
		AssetInstance->MarkPackageDirty();
	}
}

void ULODGeneratorCommandlet::SaveAsset(UObject* AssetInstance)
{
	if (!AssetInstance) return;

	if (UPackage* Package = AssetInstance->GetPackage())
	{
		if (Package->IsDirty())
		{
			FString PackageName = FPackageName::LongPackageNameToFilename(Package->GetPathName(),//
				FPackageName::GetAssetPackageExtension());

			UE_LOG(LogClass, Log, TEXT("Saving Asset to: %s"), *PackageName);

			if (Package->SavePackage(Package, AssetInstance, RF_Standalone, *PackageName, GLog))
			{
				UE_LOG(LogClass, Log, TEXT("Done"));
			}
			else
			{
				UE_LOG(LogClass, Log, TEXT("Can't Save Asset-something wrong!"));
			}
		}
	}
}
